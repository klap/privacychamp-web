# Welcome to [Privacychamp.net](https://privacychamp.net "Privacychamp")

Every internet user has to accept a privacy policy and terms of service with
every account creation or simply by using a website. Privacychamp wants to
support the internet's citizens by offering a tool that facilitates the lookup
and keeping track of all these (mostly) lengthy bodies of text that are written
in complicated language and often changed without notification.

Eventually, Privacychamp wants to offer the possibility to track certain
privacy policies and terms of use for changes and notify interested visitors.

Later, Privacychamp could serve as a platform to help understand the texts
themselves and to be able to exchange with others about its implications.

This is currently a side-project, therefore any support and feedback 
is appreciated.

The website is an Angular 7 application that is served from a Javalin server
written in Kotlin, running behind and nginx server. The backend is an 
Elasticsearch instance.

### Building
Angular app
```command line
cd pc-frontend
./build.sh
```

Javalin server
```command line
./gradlew distZip
```

