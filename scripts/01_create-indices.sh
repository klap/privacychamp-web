#!/bin/bash
curl -X PUT "localhost:9200/monsites" -H 'Content-Type: application/json' -d'
{
    "mappings" : {
        "_doc" : {
            "properties" : {
                "url" : { "type" : "keyword" },
                "documentType": {"type": "keyword" },
                "htmlRaw" : { "type" : "text" },
                "company": {"type": "text" },
                "title": {"type": "text" },
                "mainText": { "type": "text" },
                "combinedText": {"type": "text", "analyzer": "english" },
                "lang": {"type": "keyword" },
                "published": {"type": "date" },
                "applicationDate": {"type": "date" },
                "indexed": {"type": "date"},
                "indexedVersion": {"type": "integer" },
                "currentVersion": {"type": "boolean" }
            }
        }
    }
}
'

curl -X PUT "localhost:9200/searchreqs" -H 'Content-Type: application/json' -d '
{
    "mappings" : {
        "_doc": {
            "properties" : {
                "searchQuery": { "type": "keyword" },
                "userId": { "type": "keyword" },
                "ipAddress": { "type": "keyword" },
                "date": { "type": "date" }
            }
        }
    }
}
'

curl -X PUT "localhost:9200/documentreqs" -H 'Content-Type: application/json' -d '
{
    "mappings" : {
        "_doc": {
            "properties" : {
                "documentId": { "type": "keyword" },
                "userId": { "type": "keyword" },
                "ipAddress": { "type": "keyword" },
                "date": { "type": "date" }
            }
        }
    }
}
'

curl -X PUT "localhost:9200/contactmsgs" -H 'Content-Type: application/json' -d '
{
    "mappings" : {
        "_doc": {
            "properties" : {
                "email": { "type": "keyword" },
                "message": { "type": "keyword" },
                "ipAddress": { "type": "keyword" },
                "date": { "type": "date" }
            }
        }
    }
}
'
