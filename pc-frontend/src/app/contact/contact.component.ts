import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactEmail: string = "";
  contactMessage: string = "";
  confirmMessage: string = "";

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  submitMessage() {
    let submitObs = this.apiService.submitMessage(this.contactEmail, this.contactMessage);
    submitObs.subscribe((response)=>{
      //console.log("response=success");
      //console.log(response);
      this.confirmMessage = "Your message has been submitted";
    }, error => {
      console.log("error");
      console.log(error);
      //TODO notify webmaster
      this.confirmMessage = "An error occurred, please try again later";
    });
    this.contactEmail = "";
    this.contactMessage = "";
  }
}
