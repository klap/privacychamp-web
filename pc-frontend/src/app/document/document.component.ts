import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {ApiService, DocumentResponse} from "../api.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {Observable} from "rxjs";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  document: Observable<DocumentResponse>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private apiService: ApiService) { }

  ngOnInit() {
    /*this.document = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.apiService.getDocumentById(params.get('id'))
    );*/
    let id = this.route.snapshot.paramMap.get('id');
    this.document = this.apiService.getDocumentById(id);


  }

}

@Pipe({ name: 'safeHtml', pure: false})
export class SafeHtmlPipe implements PipeTransform  {
  constructor(private sanitized: DomSanitizer) {}
  transform(value) {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}
