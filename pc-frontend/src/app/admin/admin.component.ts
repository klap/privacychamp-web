import { Component, OnInit } from '@angular/core';
import {ApiService} from "../api.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  confirmMessage: string = "";

  url: string = "";
  rootSelector: string = "";
  documentType: string = "";
  company: string = "";
  lang: string = "";
  applicationDate: string = "";
  password: string = "";

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  submitIndexRequest() {
    let submitObs = this.apiService.submitIndexRequest(this.url, this.rootSelector, this.documentType, this.company, this.lang, this.applicationDate, this.password);
    submitObs.subscribe((response)=>{
      //console.log("response=success");
      //console.log(response);
      this.confirmMessage = "Indexrequest has been submitted";
    }, error => {
      //console.log("error");
      //console.log(error);
      //TODO notify webmaster
      this.confirmMessage = "An error occurred, please try again later";
    });
  }

}
