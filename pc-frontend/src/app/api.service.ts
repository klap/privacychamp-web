import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private searchURL = "api/search?q=";
  private documentURL = "api/document?id=";
  private submitMessageURL = "api/message";
  private submitIndexRequestURL = "api/indexrequest";

  constructor(private http: HttpClient) { }

  searchCall(terms: string){
    return this.http.get<SearchResponse>(this.searchURL + terms);
  }

  getDocumentById(id: string){
    return this.http.get<DocumentResponse>(this.documentURL + id);
  }

  submitMessage(contactEmail: string, contactMessage: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(this.submitMessageURL, {email:contactEmail, message:contactMessage}, httpOptions);
  }

  submitIndexRequest(url: string, rootSelector: string, documentType: string, company: string, lang: string, applicationDate: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(this.submitIndexRequestURL, {url:url, rootSelector:rootSelector, documentType:documentType, company: company, lang:lang, applicationDate:applicationDate, password:password}, httpOptions);
  }
}

export interface SearchResponse {
  success: boolean,
  result: SearchResults
}

interface SearchResults {
  count: number,
  entries: Entry[],
}

export interface Entry {
  url: string,
  company: string,
  title: string,
  type: string,
  target: string,
  snippet: string
}

export interface DocumentResponse {
  url: string,
  company: string,
  title: string,
  type: string,
  htmlRaw: string
  indexed: string
}

export interface MessageResponse {}
