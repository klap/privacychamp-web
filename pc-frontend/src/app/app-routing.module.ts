import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FindComponent} from "./find/find.component";
import {ListComponent} from "./list/list.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {DocumentComponent} from "./document/document.component";
import {ContactComponent} from "./contact/contact.component";
import {AdminComponent} from "./admin/admin.component";

const routes: Routes = [
  {
    path: 'find',
    component: FindComponent
  },
  {
    path: 'find/document/:id',
    component: DocumentComponent
  },
  {
    path: 'list',
    component: ListComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: '6pWgk4n3YbWjtYYXHuSQ6B52eHB3u4Qm',
    component: AdminComponent
  },
  {
    path: '',
    redirectTo: '/find',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
