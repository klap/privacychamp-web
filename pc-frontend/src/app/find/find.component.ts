import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ApiService, SearchResponse} from "../api.service";
import {Entry} from "../api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.css']
})
export class FindComponent implements OnInit {
  @ViewChild('searchBtn') searchBtn: ElementRef;
  //@ViewChild('alertContainer') alertContainer: ElementRef;
  alerts = [];
  inSearch = false;
  searchTerms = '';
  searchEntries: Entry[];
  nothingFound: string;

  constructor(private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    // Mock Data
    /*let e = new EntryImpl();
    e.company = "Twitch";
    e.url = "https://www.twitch.tv/p/legal/privacy-policy/";
    e.title = "Twitch.tv - Privacy Policy";
    e.type = "privacy-policy";
    e.target = "/find/document/iEG7gGkB9Y2WBhmdE8WY";
    e.snippet = "<em>Twitch</em> Twitch.tv - Privacy Policy last modified on 08/10/2018 Privacy Policy This <em>Twitch</em> Privacy Policy...(with its affiliates, “<em>Twitch</em>”) that link to this Privacy Policy (collectively, the “<em>Twitch</em> Services”...By agreeing to this Privacy Policy in your <em>Twitch</em> account setup, or by using the <em>Twitch</em> Services, you";
    this.searchEntries = [e, e, e, e, e, e, e, e, e, e];
    */
    //this.alerts = [{type: "danger", msg: "Hello world"}];
  }

  startSearch() {
    //console.log("startSearch called");
    this.searchEntries = [];
    this.inSearch = true;
    this.nothingFound = "";
    let resObs = this.apiService.searchCall(this.searchTerms);
    resObs.subscribe((data: SearchResponse) => {
      //console.log("searchCall=success");
      //console.log(data);
      this.inSearch = false;
      if(data.success){
        let result = data.result;
        let count = result.count;
        if(count == 0){
          //this.alertContainer.nativeElement.appendChild("<div>0 Results</div>");
          //this.alertContainer.nativeElement.insertAdjacentHTML('beforeend', '<div>0 Results</div>');
          //this.alerts.push({type: "danger", msg: "Nothing found"})
          this.nothingFound = "Nothing found";
        }
        this.searchEntries = result.entries;
      }else{
        //TODO
      }
    }, error => {
      //console.log("error");
      //console.log(error);
      //TODO notify webmaster
      this.nothingFound = "An error occured, please try again later";
      this.inSearch = false
    });
  }

  entryClicked(entry: Entry) {
    //console.log("entry clicked, target=" + entry.target);
    this.router.navigate([entry.target]);
  }

  closeAlert(index){
    this.alerts.splice(index, 1);
  }
}

class EntryImpl implements Entry {
  company: string;
  snippet: string;
  target: string;
  title: string;
  type: string;
  url: string;
}


