package net.privacychamp

data class Message(var email: String, var message: String)