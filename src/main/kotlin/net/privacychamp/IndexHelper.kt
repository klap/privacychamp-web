package net.privacychamp

import org.apache.http.HttpHost
import org.elasticsearch.action.ActionListener
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.common.xcontent.XContentFactory
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.net.URI
import java.net.URISyntaxException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


fun index(client: RestHighLevelClient, url: String, rootSelector: String, documentType: String, company: String = "", lang: String = "en", published: LocalDateTime?, applicationDate: LocalDateTime, indexedVersion: Int = 0, isCurrentVersion: Boolean = true){
    val doc = Jsoup.connect(url)
            .header("Accept-Language", "en-US,en;q=0.5")
            .get()
    val title = doc.title()
    val baseHref = doc.select("base").attr("href")
    val docSubset = doc.select(rootSelector)
    val links = docSubset.select("a,link")
    val domain = getDomainName(url)
    for(link in links){
        makeLinksAbsolute(link, domain, url, baseHref)
    }
    val text = docSubset.text()
    println("docSubset = $docSubset")
    //println("links = $links")
    //println("text = $text")
    val ir = IndexRequest("monsites", "_doc")
    val builder = XContentFactory.jsonBuilder()
    builder.startObject()
    builder.field("url", url)
    builder.field("documentType", documentType)
    builder.field("htmlRaw", docSubset.toString())
    builder.field("company", company)
    builder.field("title", title)
    builder.field("mainText", text)
    builder.field("combinedText", "$company $title $text")
    builder.field("lang", lang)
    builder.timeField("published", published)
    builder.field("applicationDate", applicationDate)
    builder.timeField("indexed", Date())
    builder.field("indexedVersion", indexedVersion)
    builder.field("currentVersion", isCurrentVersion)
    builder.endObject()
    ir.source(builder)
    val listener = object : ActionListener<IndexResponse> {
        override fun onResponse(indexResponse: IndexResponse) {
            println("Index Success: id=${indexResponse.id}")
        }

        override fun onFailure(e: Exception) {
            println("Index Error: msg=$e")
        }
    }
    //client.indexAsync(ir, RequestOptions.DEFAULT, listener)
    val irr = client.index(ir, RequestOptions.DEFAULT)
    println("docId=$irr.id")
}

fun makeLinksAbsolute(link: Element?, domain: String, url: String, baseHref: String) {
    link?.let {
        val target = it.attr("href")
        if(!baseHref.isNullOrEmpty()){
            it.attr("href", baseHref + target)
        }else {
            if (target.startsWith("/")) it.attr("href", domain + target)
            else {
                if (target.startsWith("#")) it.attr("href", url + target)
                else {
                }
            }
        }
    }
}

@Throws(URISyntaxException::class)
fun getDomainName(url: String): String {
    val uri = URI(url)
    return "${uri.scheme}://${uri.authority}"
}

fun main(args: Array<String>) {
    val client = RestHighLevelClient(
            RestClient.builder(
                    HttpHost("localhost", 9200, "http"),
                    HttpHost("localhost", 9201, "http")
            )
    )
    val formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
    //------
    /*index(client,
            url = "https://www.twitch.tv/p/legal/privacy-policy/",
            rootSelector = ".typeset.mobile-padded.pd-t-0",
            documentType = "privacy-policy",
            company = "Twitch",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-10-08T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://trello.com/privacy",
            rootSelector = ".layout-centered-content",
            documentType = "privacy-policy",
            company = "Trello (Atlassian)",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2019-03-14T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    //
    index(client,
            url = "https://policies.google.com/privacy",
            rootSelector = ".vwhFIf",
            documentType = "privacy-policy",
            company = "Google",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2019-01-22T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://contractfortheweb.org/privacy-policy/",
            rootSelector = ".typography.container.container-med",
            documentType = "privacy-policy",
            company = "Contract For The Web (World Wide Web Foundation)",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-10-30T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://www.speedtest.net/mobile/android/privacy.php?lang=en",
            rootSelector = ".ookla-com-privacy-policy",
            documentType = "privacy-policy",
            company = "Speedtest (Ookla)",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2019-01-25T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://www.solaredge.com/privacy-policy",
            rootSelector = ".node-content",
            documentType = "privacy-policy",
            company = "SolarEdge",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-04-01T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://www.whatsapp.com/legal/#privacy-policy",
            rootSelector = "._2yyk._-pj",
            documentType = "privacy-policy",
            company = "WhatsApp",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-04-24T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://legal.here.com/en-gb/privacy",
            rootSelector = ".l-content",
            documentType = "privacy-policy",
            company = "Here",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-05-25T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://discordapp.com/privacy",
            rootSelector = ".wrapper-2rcBnp",
            documentType = "privacy-policy",
            company = "Discord",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-05-25T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://www.fairphone.com/en/legal/fairphone-privacy-policy/",
            rootSelector = ".c-article-body.c-article-body--plain",
            documentType = "privacy-policy",
            company = "Fairphone",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2019-03-19T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://help.instagram.com/519522125107875",
            rootSelector = "._60fy",
            documentType = "privacy-policy",
            company = "Instagram (Facebook)",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-04-19T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://www.spotify.com/us/legal/privacy-policy/",
            rootSelector = ".content-main",
            documentType = "privacy-policy",
            company = "Spotify",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-05-25T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://help.netflix.com/legal/privacy",
            rootSelector = ".kb-article.kb-article-variant.gradient",
            documentType = "privacy-policy",
            company = "Netflix",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-05-11T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://protonmail.com/privacy-policy",
            rootSelector = ".container.content",
            documentType = "privacy-policy",
            company = "ProtonMail",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2019-03-22T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://www.artstation.com/privacy",
            rootSelector = ".container",
            documentType = "privacy-policy",
            company = "ArtStation",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2019-03-22T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )
    index(client,
            url = "https://duckduckgo.com/privacy",
            rootSelector = ".content-wrap",
            documentType = "privacy-policy",
            company = "DuckDuckGo",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2012-04-11T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )*/
    /*index(client,
            url = "",
            rootSelector = "",
            documentType = "privacy-policy",
            company = "",
            lang = "en",
            published = null,
            applicationDate = LocalDateTime.parse("2018-09-25T00:00:00", formatter),
            indexedVersion = 0,
            isCurrentVersion = true
    )*/
    //------
    client.close()
}
