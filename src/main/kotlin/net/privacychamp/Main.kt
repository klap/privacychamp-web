package net.privacychamp
import ch.qos.logback.classic.Level
import com.fasterxml.jackson.databind.ObjectMapper


import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.BooleanNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import io.javalin.Javalin
import io.javalin.JavalinEvent
import org.apache.http.HttpHost
import org.apache.http.util.EntityUtils
import org.elasticsearch.client.*
import org.slf4j.LoggerFactory
import java.lang.Integer.min
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

const val MAX_SEARCHRESULTS_COUNT: Int = 50

fun main(args: Array<String>) {
    val LOGGER = LoggerFactory.getLogger("root") as ch.qos.logback.classic.Logger
    LOGGER.level = Level.INFO
    /*val client = RestHighLevelClient(
            RestClient.builder(
                    HttpHost("localhost", 9200, "http"),
                    HttpHost("localhost", 9201, "http")
            )
    )*/
    val client = RestClient.builder(
            HttpHost("localhost", 9200, "http"),
            HttpHost("localhost", 9201, "http")).build()
    val hlclient = RestHighLevelClient(
            RestClient.builder(
                    HttpHost("localhost", 9200, "http"),
                    HttpHost("localhost", 9201, "http")
            )
    )
    val formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
    val port = 8080

    val pw = System.getenv("PRIVACYCHAMP_PW")
    if(pw.isNullOrEmpty()){
        LOGGER.warn("PRIVACYCHAMP_PW environment variable not set")
    }

    val app = Javalin.create().apply {
        enableCorsForAllOrigins()
        enableStaticFiles("/public")
        event(JavalinEvent.SERVER_STOPPING) {
            client.close()
        }
        enableSinglePageMode("/", "/public/index.html")
    }
            /*.requestLogger { ctx, executionTimeMs ->
                LOGGER.info("Requestlog: took $executionTimeMs millis")
            }*/
            .start(port)


    app.get("/api/search") { ctx ->
        val query = ctx.queryParam("q", "")
        val ip = ctx.ip()
        /*val ir = net.privacychamp.IndexRequest("searchreqs", "_doc")
        val builder = XContentFactory.jsonBuilder()
        builder.startObject()
        builder.field("searchQuery", query)
        builder.timeField("date", Date())
        builder.field("ipAddress", ip)
        builder.endObject()
        ir.source(builder)
        val listener = object : ActionListener<IndexResponse> {
            override fun onResponse(indexResponse: IndexResponse) {}
            override fun onFailure(e: Exception) {}
        }
        client.indexAsync(ir, RequestOptions.DEFAULT, listener)
        */
        val r = Request("POST", "/searchreqs/_doc/")
        r.setJsonEntity(
                "{" +
                  "\"searchQuery\":\"$query\"," +
                  "\"date\":\"" + formatter.format(LocalDateTime.now()) + "\"," +
                  "\"ipAddress\": \"$ip\"" +
                "}")
        val result = client.performRequestAsync(r, object: ResponseListener {
            override fun onSuccess(response: Response) {
                LOGGER.info("searchQuery index success: query=$query")
            }
            override fun onFailure(e: Exception) {
                LOGGER.error("searchquery index failure: query=$query", e)
            }
        })

        /*
        val sr = SearchRequest("monsites")
        val ssb = SearchSourceBuilder()
        val mmqb = QueryBuilders.multiMatchQuery(query, "company", "title", "combinedText")
        mmqb.field("company", 5f)
        mmqb.field("title", 3f)
        mmqb.field("combinedText")
        mmqb.fuzziness(Fuzziness.AUTO)
        ssb.query(mmqb)
        val hb = HighlightBuilder().field(
                HighlightBuilder.Field("combinedText")
                        .forceSource(true)
                        .fragmentSize(100)
                        .numOfFragments(5)
                        .highlighterType("plain")
        )
        //ssb.highlighter(HighlightBuilder().field("combinedText", 100, 5))
        ssb.highlighter(hb)
        sr.source(ssb)
        //val response = client.search(sr, RequestOptions.DEFAULT)
        //ctx.json(response)
        */
        val sr = Request("POST", "/monsites/_doc/_search?size=$MAX_SEARCHRESULTS_COUNT")
        sr.setJsonEntity(
                "{" +
                    "\"query\": {" +
                        "\"multi_match\": {" +
                            "\"query\": \"$query\"," +
                            "\"fields\": [\"company^5\", \"title^3\", \"combinedText\"]," +
                            "\"fuzziness\": \"auto\"" +
                        "}" +
                    "}," +
                    "\"highlight\": {" +
                        "\"fields\": {" +
                            "\"combinedText\": {}" +
                        "}" +
                    "}" +
                "}")
        val searchResult = client.performRequest(sr)
        val entityBody = EntityUtils.toString(searchResult.entity)
        val om = ObjectMapper()
        val json = om.readTree(entityBody)
        val resultCount = ((json as ObjectNode).get("hits") as ObjectNode).get("total").asInt()
        LOGGER.info("searchResponse for query='$query' returned $resultCount results")

        var rRoot = om.createObjectNode()
        rRoot.put("success", true)
        val rResult = om.createObjectNode()
        rResult.put("count", resultCount)
        val rEntries = om.createArrayNode()
        val hitsArray = (((json as ObjectNode).get("hits") as ObjectNode).get("hits") as ArrayNode)
        for(i in 1..min(resultCount, MAX_SEARCHRESULTS_COUNT)){
            val hit = (hitsArray[i-1] as ObjectNode)
            val o = om.createObjectNode()
            val id = (hit.get("_id") as TextNode).textValue()
            val source = (hit.get("_source") as ObjectNode)
            o.set("url", source.get("url"))
            o.set("company", source.get("company"))
            o.set("title", source.get("title"))
            o.set("documentType", source.get("documentType"))
            o.put("target", "/find/document/$id")
            val highlight = (hit.get("highlight") as ObjectNode).get("combinedText") as ArrayNode
            var highlightCombined = ""
            if(highlight.size() > 0){
                highlightCombined += (highlight.get(0) as TextNode).textValue()
            }
            if(highlight.size() > 1){
                highlightCombined += "..." + (highlight.get(1) as TextNode).textValue()
            }
            if(highlight.size() > 2){
                highlightCombined += "..." + (highlight.get(2) as TextNode).textValue()
            }
            //var highlightComined = (highlight.get(0) as TextNode).textValue() + "..." + (highlight.get(1) as TextNode).textValue() + "..." + (highlight.get(2) as TextNode).textValue()
            o.put("snippet", highlightCombined)
            rEntries.add(o)
        }
        rResult.set("entries", rEntries)
        rRoot.set("result", rResult)
        ctx.json(rRoot)
    }

    app.get("/api/document") { ctx ->
        val id = ctx.queryParam("id", "")
        val ip = ctx.ip()
        val r = Request("POST", "/documentreqs/_doc/")
        r.setJsonEntity(
                "{" +
                        "\"documentId\":\"$id\"," +
                        "\"date\":\"" + formatter.format(LocalDateTime.now()) + "\"," +
                        "\"ipAddress\": \"$ip\"" +
                        "}")
        val result = client.performRequestAsync(r, object: ResponseListener {
            override fun onSuccess(response: Response) {
                LOGGER.info("documentRequest index success: id=$id")
            }
            override fun onFailure(e: Exception) {
                LOGGER.error("documentRequest index failure: id=$id", e)
            }
        })

        val sr = Request("GET", "/monsites/_doc/$id")
        try {
            val searchResult = client.performRequest(sr)
            val entityBody = EntityUtils.toString(searchResult.entity)
            val om = ObjectMapper()
            val json = om.readTree(entityBody)
            val found = (json as ObjectNode).get("found") as BooleanNode
            if(found.booleanValue()){
                /*val document = json.get("_source") as ObjectNode
                val company = document.get("company") as String
                val title = document.get("title") as String
                val html = document.get("htmlRaw") as String
                val url = document.get("url") as String
                val indexed = document.get("indexed") as String*/

                val om = ObjectMapper()
                val source = json.get("_source") as ObjectNode
                val rootObject = om.createObjectNode()
                rootObject.set("url", source.get("url"))
                rootObject.set("company", source.get("company"))
                rootObject.set("title", source.get("title"))
                rootObject.set("documentType", source.get("documentType"))
                rootObject.set("htmlRaw", source.get("htmlRaw"))
                rootObject.set("indexed", source.get("indexed"))
                ctx.json(rootObject)
            }else{
                //TODO can this happen?
                ctx.status(404)
            }
        }catch(e: ResponseException){
            ctx.status(404)
        }
    }

    app.post("/api/message") { ctx ->
        val msg = ctx.validatedBody<Message>().getOrThrow()
        val ip = ctx.ip()
        val r = Request("POST", "/contactmsgs/_doc/")
        r.setJsonEntity(
                "{" +
                        "\"email\":\"${msg.email}\"," +
                        "\"message\":\"${msg.message}\"," +
                        "\"date\":\"" + formatter.format(LocalDateTime.now()) + "\"," +
                        "\"ipAddress\": \"$ip\"" +
                        "}")
        val result = client.performRequestAsync(r, object: ResponseListener {
            override fun onSuccess(response: Response) {
                LOGGER.info("contactMessage index success")
            }
            override fun onFailure(e: Exception) {
                LOGGER.error("contactMessage failure", e)
            }
        })
    }

    app.post("/api/indexrequest") { ctx ->
        val ir = ctx.validatedBody<IndexRequest>().getOrThrow()
        //val ip = ctx.ip()
        if(ir.password.equals(pw)) {
            LOGGER.info("indexrequest password correct")
            index(hlclient,
                    url = ir.url,
                    rootSelector = ir.rootSelector,
                    documentType = ir.documentType,
                    company = ir.company,
                    lang = ir.lang,
                    published = null,
                    applicationDate = LocalDateTime.parse(ir.applicationDate, formatter),
                    indexedVersion = 0,
                    isCurrentVersion = true
            )
        }else{
            LOGGER.warn("indexrequest password incorrect: ${ir.password}")
        }
    }
}

data class IndexRequest(var url: String, var rootSelector: String, var documentType: String, var company: String, var lang: String, var applicationDate: String, var password: String)
